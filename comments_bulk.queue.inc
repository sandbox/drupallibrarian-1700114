<?php
/*
 * @file
 */

/**
 * Implements hook_cron().
 */
function comments_bulk_cron() {
  $unpublished_only = TRUE;
  if (variable_get('comments_bulk_admin_scheduler_unpublished') == 1) {
    $unpublished_only = FALSE;
  }
  $days = variable_get('comments_bulk_admin_scheduler_days', '0');
  $months = variable_get('comments_bulk_admin_scheduler_months', '0');
  $year = variable_get('comments_bulk_admin_scheduler_year', '0');
  $cids = comments_bulk_get_cids($unpublished_only, $days, $months, $year);
  watchdog('comments_bulk', '%number comments deleted.', array('%number' => count($cids)));
	$enabled = variable_get('comments_bulk_admin_scheduler_enable');
	if (isset($cids) && isset($enabled)) {
		$queue = DrupalQueue::get("comments_bulk_delete_queue");
		$limit = variable_get('comments_bulk_admin_limit', '100');
  	$comment_chunks = array_chunk($cids, $limit);
  	foreach ($comment_chunks as $chunk) {	
    	$queue->createItem($chunk);
  	}
		//watchdog('Actions', '%number comments deleted.', array('%number' => count($cids)));
	}
//watchdog('content', '%number comments deleted.', array('%number' => count($cids)));
}

/*
 * Implements hook_cron_queue_info().
 */
function comments_bulk_cron_queue_info() {
	$queues = array();
    $queues['comments_bulk_delete_queue'] = array(
      'worker callback' => 'comments_bulk_delete_worker',
      'time' => 30,
    );
		return $queues;
}

/*
 * Get comment ids according to the config settings.
 */
function comments_bulk_get_cids($unpublished = TRUE, $days = '0', $months = '0', $year = '0') {
  // Set up the comments age
  $comments_age = '';
  $time = NULL;
  if (!empty($year)) {
    $comments_age .= '-' . $year . ' year ';
  }
  if (!empty($months)) {
    $comments_age .= '-' . $months . ' months ';
  }
  if (!empty($days)) {
    $comments_age .= '-' . $days . ' days';
  }
  if (!empty($comments_age)) {
    $time = strtotime($comments_age);
  }
  $cids = comments_bulk_select_comments($unpublished, $time);
	return $cids;
}

/*
 * Worker callback defined in hook_cron_queue_info()
 */
function comments_bulk_delete_worker($item) {
  $number = count($item);
  comment_delete_multiple($item);
}