<?php

/**
 * @file
 * Drush integration for Comments bulk delete module.
 */

/*
 * Implements hook_drush_command()
 */
function comments_bulk_drush_command() {
	$items = array();
	$items['comments-bulk-delete'] = array(
		'description' => "Delete comments in bulk",
		'options' => array(
			'all' => 'Whether to delete all comments (if empty, defaults to unpublished)',
			'days' => 'Delete comments older than specified days',
			'months' => 'Delete comments older than specified months',
			'year' => 'Delete comments older than specified year',
),
		'examples' => array(
			'drush cbd --all' => 'Delete all comments, published and unpublished',
			'drush cbd --months=1' => 'Delete unpublished comments older than one month',
		),
		'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
		'aliases' => array('cbd'),
	);
	return $items;
}

/*
 * Implements drush_hook_command_validate().
 */
function drush_comments_bulk_delete_validate() {
	
}

/*
 * Implements drush_hook().
 */
function drush_comments_bulk_delete() {
  $unpublished = TRUE;
  if (drush_get_option('all')) {		
		$unpublished = FALSE;
	}
  $time = '';
  $year = drush_get_option('year');
  if (!empty($year)) {
    $time .= '-' . $year . ' year ';
  }
  $months = drush_get_option('months');
  if (!empty($months)) {
    $time .= '-' . $months . ' months ';
  }
	$days = drush_get_option('days');
  if (!empty($days)) {
    $time .= '-' . $days . ' days';
  }
	$age = strtotime($time);
	$cids = comments_bulk_select_comments($unpublished, $age);
	$number = count($cids);
	print t("@number comment(s) deleted\n", array('@number' => $number));
	comment_delete_multiple($cids);
}