<?php
/**
 * @file
 * Administration and settings forms.
 */

/*
 * Comments operations form.
 */

function comments_bulk_delete_form($form, &$form_state) {
  $form = array();
  $options = array(
    'all' => t('Delete all comments'),
    'unpublished' => t('Delete only unpublished comments'),
  );
  $form['options'] = array(
    '#type' => 'radios',
    '#title' => 'Bulk delete comments',
    '#options' => $options,
    //'#theme' => array('comments_bulk_delete'),
  );
  $unpublished_cids = count(comments_bulk_select_comments(TRUE));
  $published_cids = count(comments_bulk_select_comments());
  $form['unpublished_count'] = array(
    '#type' => 'hidden',
    '#value' => $unpublished_cids,
  );
  $form['published_count'] = array(
    '#type' => 'hidden',
    '#value' => $published_cids,
  );

  // Setup the confirmation page
  if (isset($form_state['storage']['confirm'])) {

    return confirm_form($form, 'Do you really want to delete the comments?', 'admin/content/comments_bulk', 'All comments are going to be deleted', '', 'Cancel');
  }
  else {
    // Don't show submit button on the confirmation page
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete comments'),
    );

	return $form;
	
  }

}

function comments_bulk_delete_form_submit($form, &$form_state) {

  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form
    $form_state['rebuild'] = TRUE; // along with this
  }
  else {
  $op = $form_state['values']['options'];

  switch ($op) {
    case 'all' :
      // Select all comments to be deleted
      $cids = comments_bulk_select_comments();
      break;
    case 'unpublished':
      // Select all unpublished comments to be deleted
      $cids = comments_bulk_select_comments($unpublished = TRUE);
      break;
  }
  // Set up the Batch API
  $batch = array(
    'finished' => '_comments_bulk_process_finished',
    'title' => t('Deleting comments'),
    'init_message' => t('Starting to delete comments'),
    'progress_message' => '',
    'error_message' => t('An error occurred and some or all of the exports have failed.'),
  );
  // Use Batch API to delete comments
  $total_cids = count($cids);
  $limit = variable_get('comments_bulk_admin_limit');
  $comment_chunks = array_chunk($cids, $limit);
  foreach ($comment_chunks as $chunk) {
    $batch['operations'][] = array('comments_bulk_delete_process', array($chunk, count($chunk), $total_cids));
  }
  if (!empty($batch['operations'])) {
    batch_set($batch);
  }
  }
}
/*
 * Comments bulk module settings page.
 */
function comments_bulk_settings($form, &$form_state) {
  $form = array();
  $form['comments_bulk_admin_limit'] = array(
    '#type' => 'textfield',
    '#title' => 'Maximum comments per operation',
    '#size' => 4,
    '#description' => 'Set maximum comment number to be processed per operation (too many can have a hit on the system).',
    '#default_value' => variable_get('comments_bulk_admin_limit', 100),
  );
  $enabled = variable_get('comments_bulk_admin_scheduler_enable');
  $form['comments_bulk_scheduler'] =  array(
    '#type' => 'fieldset',
    '#title' => 'Comments bulk scheduler settings',
    '#collapsible' => TRUE,
    '#collapsed' => !empty($enabled) ? FALSE : TRUE,
  );

  $form['comments_bulk_scheduler']['comments_bulk_admin_scheduler_enable'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable comments removal on schedule',
    '#default_value' => variable_get('comments_bulk_admin_scheduler_enable', ''),
    '#description' => t('Choose how old the comments must be before they\'re deleted. Leave blank for all.'),
  );
	  $form['comments_bulk_scheduler']['comments_bulk_admin_scheduler_unpublished'] = array(
    '#type' => 'checkbox',
    '#title' => 'Delete all comments',
    '#default_value' => variable_get('comments_bulk_admin_scheduler_unpublished', ''),
    '#description' => t('If unchecked, only unpublished comments will be deleted.'),
  );
  $form['comments_bulk_scheduler']['comments_bulk_admin_scheduler_days'] = array(
    '#type' => 'textfield',
    '#title' => 'Days: ',
    '#size' => 4,
    '#default_value' => variable_get('comments_bulk_admin_scheduler_days', 0),
  );
  $form['comments_bulk_scheduler']['comments_bulk_admin_scheduler_months'] = array(
    '#type' => 'textfield',
    '#title' => 'Months: ',
    '#size' => 4,
    '#default_value' => variable_get('comments_bulk_admin_scheduler_months', 0),
  );
  $form['comments_bulk_scheduler']['comments_bulk_admin_scheduler_year'] = array(
    '#type' => 'textfield',
    '#title' => 'Year(s): ',
    '#size' => 4,
    '#default_value' => variable_get('comments_bulk_admin_scheduler_year', 0),
  );
  //$form['#submit'][] = 'comments_bulk_settings_submit';
  $form['#attached']['css'] = array(drupal_get_path('module', 'comments_bulk') . '/comments_bulk.css');
  return system_settings_form($form);
}

function comments_bulk_settings_validate($form, &$form_state) {
 
  $values = $form_state['values'];
  $limit = $values['comments_bulk_admin_limit'];
  $days = $values['comments_bulk_admin_scheduler_days'];
  $months = $values['comments_bulk_admin_scheduler_months'];
  $year = $values['comments_bulk_admin_scheduler_year'];
  $enabled = $values['comments_bulk_admin_scheduler_enable'];
  if (!empty($limit) && !is_numeric($limit)) {
    form_set_error('comments_bulk_admin_limit', t('Maximum comments must be a number'));
  }
  if ($enabled == 1) {
    if (!empty($days) && !is_numeric($days)) {
      form_set_error('comments_bulk_admin_scheduler_days', t('Days must be a number'));
    }
    elseif (!empty($months) && !is_numeric($months)) {
      form_set_error('comments_bulk_admin_scheduler_months', t('Months must be a number'));
    }
    elseif (!empty($year) && !is_numeric($year)) {
      form_set_error('comments_bulk_admin_scheduler_year', t('DYear(s) must be a number'));
    }
  }
}


function theme_comments_bulk_delete_form($variables) {

  $form = $variables['form'];
  // Disable buttons if there're no comments
  $unpublished_cids = $form['unpublished_count']['#value'];
  $published_cids = $form['published_count']['#value'];
  if ($unpublished_cids == 0) {
    $form['options']['unpublished']['#attributes']['disabled'] = 'disabled';
  }

  if ($published_cids == 0) {
    $form['options']['all']['#attributes']['disabled'] = 'disabled';
  }
  $form['options']['unpublished']['#title'] .= ' ' . t('(total:') . ' ' . $unpublished_cids . ')';
  $form['options']['all']['#title'] .= ' ' . t('(total:') . ' ' . $published_cids . ')';
  $output = drupal_render_children($form);
  return $output;
}